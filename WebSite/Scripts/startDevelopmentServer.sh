#!/bin/bash

cd "$(dirname "$0")"

DOCKER_COMPOSE_FILE='../docker-compose.yml'

docker-compose -f $DOCKER_COMPOSE_FILE down -v --remove-orphans
docker-compose -f $DOCKER_COMPOSE_FILE up -d --build

echo "Wait 3s..."
sleep 3s

./restoreDump.sh 50_users.sql
./restoreDump.sh 60_dump.sql

docker-compose -f $DOCKER_COMPOSE_FILE logs -f
