
from django.urls import re_path
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.authtoken import views
from webservice import views as webserviceViews

urlpatterns = [
    re_path(r'^token-auth/$',                    webserviceViews.CustomObtainAuthToken.as_view()),
    re_path(r'^users/$',                         webserviceViews.UserList.as_view()),
    re_path(r'^users/(?P<pk>[0-9]+)/$',          webserviceViews.UserDetail.as_view()),
    re_path(r'^users/(?P<pk>[0-9]+)/spots/$',    webserviceViews.UserDetailSpotList.as_view()),
    re_path(r'^users/(?P<pk>[0-9]+)/pictures/$', webserviceViews.UserDetailPicturesList.as_view()),
    re_path(r'^spots/$',                         webserviceViews.SpotList.as_view()),
    re_path(r'^spots/(?P<pk>[0-9]+)/$',          webserviceViews.SpotDetail.as_view()),
    re_path(r'^spots/byDistance/$',              webserviceViews.SpotListByDistance.as_view()),
    re_path(r'^pictures/$',                      webserviceViews.PictureList.as_view()),
    re_path(r'^pictures/(?P<pk>[0-9]+)/$',       webserviceViews.PictureDetail.as_view()),
    re_path(r'^pictures/byNewest/$',             webserviceViews.PictureListByNewest.as_view()),
    re_path(r'^webservice/$',                    webserviceViews.Webservice.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
